/**
 * Selectbox
 * @author Johan
 *
 * Simple plugin to replace a <select> input element with other HTML elements
 * to be able to apply specific styles to each part.
 *
 * Usage:
 * $('select').Selectbox();
 * $('select.fancy').Selectbox({
 *	 // specific settings
 * });
 *
 * Settings:
 * - elements.* array jQuery constructor arguments array to create each part of the Selectbox.
 *   possible keys are: display, dropdown, optgroup, label, list and option.
 * - hiddenClass string ClassName that hides the <select> element.
 * - focusClass string ClassName applied to the display element when the <select> has focus.
 * - selectedClass string ClassName applied to the selected option element
 */
jQuery(function($) {
	$.fn.Selectbox = function(parameters) {
		if (typeof parameters == "string") {
			// special commands
			switch (parameters) {
			  case "remove":
			  case "destroy":
				this.trigger("Selectbox.destroy");
				break;
			}
			return;
		} else {
			var settings = $.extend({
				// jQuery constructor arguments for each part of the Selectbox:
				elements: {
					display: [ "<span>", {
						"class": "selectbox-display"
					} ],
					dropdown: [ "<div>", {
						"class": "selectbox-dropdown"
					} ],
					optgroup: [ "<li>", {
						"class": "selectbox-optgroup"
					} ],
					label: [ "<strong>" ],
					list: [ "<ul>", {
						"class": "selectbox-list"
					} ],
					option: [ "<li>", {
						"class": "selectbox-option"
					} ]
				},
				// className applied to the <select> element to hide it
				hiddenClass: "selectbox-hidden",
				// className for the display element when the selectbox has focus
				focusClass: "selectbox-focus",
				// className for the selected option:
				selectedClass: "selected"
			}, parameters);
		}
		return this.each(function() {
			var select = $(this), options = [], className = select.attr("class"), displayElement = $.apply(this, settings.elements.display).on("click", function() {
				select.trigger("focus");
			}).insertAfter(select), dropdownElement = $.apply(this, settings.elements.dropdown).insertAfter(displayElement).hide(), form = select.closest("form"), isOwnEvent = false, defaultOptionClasses = function() {
				var args = settings.elements.option;
				if (args.length > 1 && args[1]["class"]) {
					return args[1]["class"].split(" ");
				}
				return [];
			}(), selectedClass, timer;
			if (className) {
				dropdownElement.addClass(className);
				displayElement.addClass(className);
			}
			select.addClass(settings.hiddenClass);
			// recursive function to generate the items in the drop down list:
			function createOptions(items) {
				var elements = [];
				items.each(function() {
					var item = $(this), type = this.tagName.toLowerCase(), option, className;
					if (type == "optgroup") {
						option = $.apply(this, settings.elements.optgroup);
						if (item.attr("label")) {
							option.append($.apply(this, settings.elements.label).html(item.attr("label")));
						}
						option.append($.apply(this, settings.elements.list).append(createOptions(item.children())));
						elements.push(option);
					} else {
						option = $.apply(this, settings.elements.option).data("value", item.attr("value") || "").html(item.html() || "&nbsp;");
						className = item.attr("class");
						if (className) {
							option.addClass(className);
						}
						options = options.add(option);
						elements.push(option);
					}
				});
				return elements;
			}
			// updates the display element, and selects the current option:
			function setDisplayValue() {
				var option = $(options).removeClass(settings.selectedClass).eq(select.prop("selectedIndex")), className = function(classNames) {
					classNames = $(classNames.split(" ")).filter(function() {
						return defaultOptionClasses.indexOf("" + this) == -1;
					});
					return classNames.get().join(" ");
				}(option.attr("class") || "");
				option.addClass(settings.selectedClass);
				displayElement.html(option.html());
				if (selectedClass) {
					displayElement.removeClass(selectedClass);
				}
				if (className) {
					displayElement.addClass(className);
					selectedClass = className;
				}
			}
			form.on("reset.Selectbox", function() {
				window.setTimeout(function() {
					select.trigger("change.Selectbox");
				});
			});
			dropdownElement.on("scroll", function() {
				window.clearTimeout(timer);
				timer = window.setTimeout(function() {
					isOwnEvent = false;
					select.trigger("focus");
				}, 100);
			}).on("mousedown", function() {
				window.clearTimeout(timer);
				isOwnEvent = true;
			}).on("mouseup", function() {
				isOwnEvent = false;
				select.trigger("focus");
			}).on("click", function() {
				var value;
				window.clearTimeout(timer);
				if (!document.all) {
					select.trigger("focus");
				}
				if (event.target) {
					value = $(event.target).data("value");
					if (typeof value != "undefined") {
						select.val(value);
						select.trigger("change");
						dropdownElement.hide();
					}
				}
				return false;
			});
			// custom event to trigger the selectbox to update itself:
			select.on("update.Selectbox", function() {
				options = $([]);
				dropdownElement.empty().append($.apply(this, settings.elements.list).append(createOptions(select.children())));
				setDisplayValue();
				select.data("isSelectbox", true);
			}).on("destroy.Selectbox", function() {
				dropdownElement.remove();
				displayElement.remove();
				select.removeClass(settings.hiddenClass).off(".Selectbox");
			}).on("keydown.Selectbox", function(event) {
				if (dropdownElement.is(":visible")) {
					switch (event.which) {
					  case 13:
						// enter
						dropdownElement.hide();
						break;

					  case 38:
					  // up
						case 40:
						// down
						select.trigger("change");
						break;
					}
				} else {
					if (event.which == 32) {
						// spacebar
						dropdownElement.show();
					}
				}
			}).on("change.Selectbox", function() {
				setDisplayValue();
			}).on("focus.Selectbox", function() {
				window.clearTimeout(timer);
				displayElement.addClass(settings.focusClass);
				dropdownElement.show();
			}).on("blur.Selectbox", function() {
				if (!isOwnEvent) {
					displayElement.removeClass(settings.focusClass);
					timer = window.setTimeout(function() {
						dropdownElement.hide();
					}, 100);
				}
			}).trigger("update.Selectbox");
		});
	};
	// Hooks into the val() method of jQuery to be able to update after val() was used to set the new value
	$.valHooks.select = {
		set: function(select) {
			select = $(select);
			if (select.data("isSelectbox")) {
				window.setTimeout(function() {
					select.trigger("change.Selectbox");
				});
			}
		}
	};
});